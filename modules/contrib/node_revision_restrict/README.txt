MODULE
------
Node Revision Restrict

DESCRIPTION/FEATURES
--------------------
* This module intends to restrict node revisions. 
It has admin interface to specify node revision threashold for each 
content type available.
Any new revision added beyond specifeid threashold will delete the oldest
revision availabe. 

IDEALLY SUITED FOR:
-------------------

Sites with limited database size, this module may help you recover space and
improve site performance.

REQUIREMENTS
------------
Drupal 7.0

INSTALL/CONFIG
--------------
See http://drupal.org/getting-started/install-contrib for instructions on
how to install or update Drupal modules.

Once module is installed and enabled you may navigate
to <www.example.com/admin/config/content/node_revision_restrict>.
and set the revision thresholds for the content types available.

CREDITS
--------

This module was created by OSSCube Solutions Pvt Ltd <www dot osscube dot com>
Developed by shivendu kumar ray <shivendu at osscube dot com>
